package hk.com.novare.maxmindcrunch;

import com.fasterxml.jackson.databind.JsonNode;
import com.maxmind.db.Reader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.crunch.*;
import org.apache.crunch.fn.Aggregators;
import org.apache.crunch.impl.mr.MRPipeline;
import org.apache.crunch.types.writable.Writables;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;

/**
 * This product includes GeoLite2 data created by MaxMind, available from
 * <a href="http://www.maxmind.com">http://www.maxmind.com</a>.
 *
 *
 * Crunch follows the Standard Hadoop tool pattern.
 */
public class MaxmindCrunchJob extends Configured implements Tool, Serializable {

    private static final Log LOG = LogFactory.getLog(MaxmindCrunchJob.class);

    /**
     * The GeoLite2 database.
     * Download from http://dev.maxmind.com/geoip/geoip2/geolite2/
     */
    public static final String MAXMIND_PATHNAME = "GeoLite2-City.mmdb";

    /**
     * MaxMind reader
     */
    private transient Reader reader;


    /**
     * Standard Hadoop tool run()
     *
     * @param args
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public int run(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        // Debugging: output the configuration
        LOG.info("Troubleshooting: tmpfiles=" + getConf().get("tmpfiles"));


        // Set up the MaxMind reader.
        // This must be distributed properly with -files since the MaxMind code doesn't know how to use HDFS.
        reader = new Reader(new File(MAXMIND_PATHNAME));

        Pipeline pipeline = new MRPipeline(MaxmindCrunchJob.class, getConf());

        PCollection<String> lines = pipeline.readTextFile(args[0]);


        // Aggregator that will sum the count per country+hour.
        final Aggregator<Long> longAggregator = Aggregators.SUM_LONGS();


        // Extract country code+hour and count, group by code+hour, sum the values.
        final PTable<String, Long> countryCodeHourCount = lines.parallelDo(extractCountryCodeHourCount,
                Writables.tableOf(Writables.strings(), Writables.longs())).groupByKey().combineValues(longAggregator);

        // Write out to a text file
        pipeline.writeTextFile(countryCodeHourCount, args[1]);

        // Start processing.
        pipeline.done();

        // Always return 0?
        return 0;
    }

    final DoFn<String, Pair<String, Long>> extractCountryCodeHourCount = new DoFn<String, Pair<String, Long>>() {
        @Override
        public void process(String line, Emitter<Pair<String, Long>> pairEmitter) {
            // Split fields by spaces

            final String[] fields = line.split(" ");

            // Process if 4 or more fields found
            if (fields.length > 3) {
                final String ipAddress = fields[0];
                final String timestamp = fields[3];

                // Look up IP address using MaxMind
                JsonNode jsonNode = null;
                try {
                    jsonNode = reader.get(InetAddress.getByName(ipAddress));
                } catch (IOException e) {
                    // shouldn't happen?
                    // XXX Handle exception properly
                    return;
                }

                if (jsonNode != null) {
                    final String countryCode = jsonNode.get("country").get("iso_code").asText();

                    final String hour = timestamp.substring(13, 15);

                    if (hour != null) {
                        // Pair of countryCode + hour, and 1 instance of it
                        pairEmitter.emit(Pair.<String, Long>of(countryCode + hour, 1L));
                    }
                }

                // Alternative for testing.
                // Just use PH

//                final String hour = timestamp.substring(13, 15);
//
//                if (hour != null) {
//                    pairEmitter.emit(Pair.<String, Long>of("PH" + hour, 1L));
//                }


            }

        }
    };


    /**
     * Standard Hadoop tool main()
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new MaxmindCrunchJob(), args);
        System.exit(res);
    }

}
